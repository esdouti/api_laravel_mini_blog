<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Illuminate\Support\Str;
use App\Services\Livre;

class BasicTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_example()
    {
        // $data = [10, 20, 30];
        // $result = array_sum($data);
        // $this->assertEquals(60, $result);


        // $data = 'Je suis petit';
        // $this->assertTrue(Str::startsWith($data, 'Je'));
        // $this->assertFalse(Str::startsWith($data, 'Tu'));
        // $this->assertSame(Str::startsWith($data, 'Tu'), false);
        // $this->assertStringStartsWith('Je', $data);
        // $this->assertStringEndsWith('petit', $data);


        // $response = $this->get('/test');
        // $response->assertSuccessful();
        // $this->assertEquals('coucou', $response->getContent());


        // $response = $this->get('/test1');
        // $response->assertViewHas('message', 'Vous y êtes !');


        // $response = $this->get('test2');
        // $response->assertStatus(200);



        // Création Mock
        $this->mock(Livre::class, function ($mock) {
            $mock->shouldReceive('getTitle')->andReturn('Titre');
        });
        // Action
        $response = $this->get('test2');
        // Assertions
        $response->assertSuccessful();
        $response->assertViewHas('titre', 'Titre');

    }
}
