<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\apiUserController;
use App\Http\Controllers\apiMovieController;
use App\Http\Controllers\apiCategoryController;




/*
|--------------------------------------------------------------------------
| API Routes;
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::get('movies',[apiMovieController::class,'getData']);
Route::get('movies/{id?}',[apiMovieController::class,'getData2']);
Route::get('movies/cat/{cat}',[apiMovieController::class,'getData3']);
Route::get('categories/{id?}',[apiCategoryController::class,'getData2']);
Route::get('data',[apiUserController::class,'getData']);
//Route::get('users',[apiUserController::class,'getUser']); //get all users
//Route::get('users/{id}',[apiUserController::class,'getUser1']); //get a specific user with a param
Route::get('users/{id?}',[apiUserController::class,'getUser2']); //get a specific user with a param or all is params don't exists
Route::post('users',[apiUserController::class,'register']);
Route::post('login',[apiUserController::class,'login']);
Route::put('users/{id}',[apiUserController::class,'edit']);
Route::delete('users/{id}',[apiUserController::class,'delete']);


