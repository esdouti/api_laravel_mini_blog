<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\MovieController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\TestController;
use App\Models\User;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
Route::get('/',[UserController::class,'index']);
Route::get('/test', function () {
    return 'coucou';
});
Route::get('/test1', function () {return view('test')->with('message', 'Vous y êtes !');});
Route::get('/test2', [TestController::class,'index']);


Route::get('/login',[UserController::class,'login1']);
Route::get('/users',[UserController::class,'users']);
Route::get('/categories',[CategoryController::class,'categories']);
Route::get('/movies',[MovieController::class,'movies']);
Route::post('/register',[UserController::class,'register']);
Route::post('/new_cate',[CategoryController::class,'new_cate']);
Route::post('/new_movie',[MovieController::class,'new_movie']);
Route::post('/back_login',[AdminController::class,'back_login']);
Route::post('/edit_cate',[CategoryController::class,'edit']);
Route::post('/delete_cate',[CategoryController::class,'delete']);
Route::post('/delete_movie',[MovieController::class,'delete']);
Route::post('/edit_movie',[MovieController::class,'edit']);
Route::post('/delete_user',[UserController::class,'delete']);




