@extends('layouts.adminLayout')
@section('css')
<link rel="stylesheet" type="text/css" href="assets/css/datatables.css">
<link rel="stylesheet" href="assets/css/swiper.min.css">
<!--Page Specific -->
<link rel="stylesheet" type="text/css" href="assets/css/nice-select.css">
@endsection
@section('content')
                <div class="row">
                    <div class="col xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="page-title-wrapper">
                            <div class="page-title-box">
                                <h4 class="page-title">Liste des utilisateurs</h4>

                            </div>
                            {{-- <div class="breadcrumb-list">
                                <button type="button" class="btn btn-secondary mt-2 mr-2" data-bs-toggle="modal" data-bs-target="#exampleModalLong">Ajouter un utilisateur</button>

                            </div> --}}
                        </div>
                    </div>
                </div>
                <!-- Products view Start -->
				<div class="row">
                    <!-- Styled Table Card-->
                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                        <div class="card table-card">
                            <div class="card-header pb-0">
                                <h4>Default Datatable</h4>
                            </div>
                            <div class="card-body">
                               <table id="example" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
									<thead>
										<tr class="text-center">
											<th>First name</th>
											<th>Last name</th>
											<th>Email</th>
											<th>Action</th>
										</tr>
									</thead>
									<tbody>
                                        @foreach ($users as $user)
                                        <?php
                                        $username = $user->firstname.'   '.$user->lastname;
                                        ?>
                                            <tr class="text-center">
                                                <td>{{$user->firstname}}</td>
                                                <td>{{$user->lastname}}</td>
                                                <td>{{$user->email}}</td>
                                                <td>
                                                    {{-- <span><i class="icofont-ui-edit"></i></span> --}}
                                                    <span data-cat_id="{{$user->id}}"  data-fnom="{{$username}}" data-bs-toggle="modal" data-bs-target="#exampleModalLong2"><i class="icofont-ui-delete"></i></span>
                                                </td>
                                            </tr>
                                        @endforeach
									</tbody>
								</table>
                            </div>
                        </div>
                    </div>
                </div>
				<div class="ad-footer-btm">
					<p>Copyright 2022 © SplashDash All Rights Reserved.</p>
				</div>

                <!-- Model -->
	<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <form action="/register" method="post" enctype="multipart/form-data">
            @csrf
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Nouvel utilisateur</h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                <div class="row">
                    <label class="col-2" for="">Nom: </label>
                    <input class="col-10" type="text" name="lastname" required>
                </div>
                <div class="row">
                    <label class="col-2" for="">Prenom: </label>
                    <input class="col-10" type="text" name="firstname" required>
                </div>
                <div class="row">
                    <label class="col-2" for="">Email: </label>
                    <input class="col-10" type="email" name="email" required>
                </div>
                <div class="row">
                    <label class="col-2" for="">Password: </label>
                    <input class="col-10" type="text" name="password" required>
                </div>
                <div class="row">
                    <label class="col-2" for="">Confirmation Password: </label>
                    <input class="col-10" type="text" name="confirmation" required>
                </div>
                <div class="row">
                    <label class="col-2" for="">Picture: </label>
                    <input class="col-10" type="file" name="picture">
                </div>
                </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                <button type="submit" class="btn btn-primary">Enregistrer</button>
                </div>
            </div>
            </div>
        </form>
    </div>
     {{-- DELETEMODAL --}}
     <div class="modal fade" id="exampleModalLong2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
        <form action="/delete_user" method="POST">
            @csrf
            {{-- {{ method_field('PUT') }} --}}
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle"></h5>
                <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                </div>
                <div class="modal-body">
                   <p>Confirmez la suppression de l'utilisateur: <b class="modal-cate"> </b></p>
                    <input type="hidden" id="cat_id" name="cat_id">


                  </div>
                <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Fermer</button>
                <button type="submit" class="btn btn-primary">Valider</button>
                </div>
            </div>
            </div>
        </form>
    </div>
    {{-- DELETEMODAL --}}
@endsection
@section('js')
  <script src="assets/js/datatables.min.js"></script>
  <script src="assets/js/dataTables.responsive.min.js"></script>
  <script>
  $(document).ready(function() {
      $('#example').DataTable();
  } );
  </script>
  <script>
    /*SCRIPT Du delete DE CATEGORIE*/
    $('#exampleModalLong2').on('show.bs.modal', function(event){
        var button = $(event.relatedTarget)
        var name = button.data('fnom')
        var cat_id = button.data('cat_id')
        console.log(name);

        var modal = $(this)

        modal.find('.modal-title').text("SUPPRIMER L'UTILISATEUR");
        modal.find('.modal-cate').text(name);
        modal.find('.modal-body #cat_id').val(cat_id);
    })
    /*SCRIPT Du delete DE CATEGORIE*/
</script>
@endsection
