<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Movie;
use App\Models\Category;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{

    function index(){
        $user=User::all()->count();
        $movies=Movie::all()->count();
        $categories=Category::all()->count();
        return view('index',["users"=>$user,"movies"=>$movies,"categories"=>$categories]);
    }

    function users() {
        $users = User::all();
        return view('users',['users'=>$users]);
    }


    function login1(){
        return view('login');
    }


    function getData(){
        return ["name" => "espoir","email"=>"diespoir16@gmail.com","address"=>"1 rue des écoles"];
    }

    //get all users
    // function getUser(){
    //     $data = User::all();
    //     return $data;
    // }


    //get a specific user with a param
    // function getUser1($id){
    //     return User::find($id);
    // }

    function getUser2($id=null){
        return $id?User::find($id):User::all();
    }




    //Register
    public function register (Request $request){

        $validated = $request->validate([
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|min:3',
            'picture' => 'sometimes|image'
        ]);
        if($request->password == $request->confirmation){
            $user = new User;
            $user->firstname=$request->firstname;
            $user->lastname=$request->lastname;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            //Uplod de l'img
            if (request()->hasFile('picture')) {
                $avatar = request()->file('picture')->getClientOriginalName();
                request()->file('picture')->storeAs('public',$user->id . '/' . $avatar, '');
                $user->picture=$avatar;
            }else{
                $avatar='default.png';
                $user->picture=$avatar;
            }
            //fin uplod
            $user->save();
            $users = User::all();
            return view('users',['users'=>$users]);
                }
        else{
            return ['error'=>'Erreur du mot de passe'];
        }
    }




    //Login
    public function login(Request $request){

        $validated = $request->validate([
           'email' => 'required|email',
           'password' => 'required|min:3'
        ]);
        $user_data = array(
           'email'  => $request->get('email'),
           'password' => $request->get('password')
        );
        if(Auth::attempt($user_data))
        {
           return ['status'=>'Login success'];
        }
        else
        {
           return ['error'=>'Identifiants incorrects'];
        }
    }


    function delete(Request $request){
        $id=$request->cat_id;
        $user=User::find($id);
        $user->delete();
        return redirect('/users');
    }














}
