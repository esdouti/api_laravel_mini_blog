<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;


class apiUserController extends Controller
{
    function getData(){
        return ["name" => "espoir","email"=>"diespoir16@gmail.com","address"=>"1 rue des écoles"];
    }


    //get all users
    // function getUser(){
    //     $data = User::all();
    //     return $data;
    // }


    //get a specific user with a param
    // function getUser1($id){
    //     return User::find($id);
    // }

    function getUser2($id=null){
        return $id?User::find($id):User::all();
    }




    //Register
    public function register (Request $request){

        $validated = $request->validate([
            'firstname' => 'required|max:255',
            'lastname' => 'required|max:255',
            'email' => 'required|email',
            'password' => 'required|min:3',
            'picture' => 'sometimes|image'
        ]);
        if($request->password == $request->confirmation){
            $user = new User;
            $user->firstname=$request->firstname;
            $user->lastname=$request->lastname;
            $user->email=$request->email;
            $user->password=Hash::make($request->password);
            //Uplod de l'img
            if (request()->hasFile('picture')) {
                $avatar = request()->file('picture')->getClientOriginalName();
                request()->file('picture')->storeAs('public',$user->id . '/' . $avatar, '');
                $user->picture=$avatar;
            }else{
                $avatar='default.png';
                $user->picture=$avatar;
            }
                //fin upload
                try{
                    $user->save();
                    return ["user"=>$user,'message'=>'Utilisateur ajouté avec succès !!!'];
                }catch(\Exception $e){
                    // return $e->getMessage();
                    return ['message'=>'Utilisateur existe déjà !!!'];

                }
            // }catch(\Exception $e){
            //     return $e->getMessage();
            // }

            }
        else{
            return ['error'=>'Erreur du mot de passe'];
        }
    }




    //Login
    public function login(Request $request){

        $validated = $request->validate([
           'email' => 'required|email',
           'password' => 'required|min:3'
        ]);
        $user_data = array(
           'email'  => $request->get('email'),
           'password' => $request->get('password')
        );
        if(Auth::attempt($user_data))
        {
            $user = Auth::user();
           return ['user'=>$user,'message'=>'Login success'];
        }
        else
        {
           return ['message'=>'Identifiants incorrects'];
        }
    }


    public function delete($id){
        $user = User::find($id);
        if($user){
            $user->delete();
            return ["messge" => "User deleted successfully"];
        }else{
            return ["messge" => "User does not exists"];
        }
    }


    public function edit (Request $request, $id){
        $user = User::find($id);

        if($request->password){
            if($request->password == $request->confirmation){
                $user->password=Hash::make($request->password);
            }else{
                return ["messge" => "Passwords does not match"];
            }
        }

        if($request->firstname){$user->firstname = $request->firstname;}
        if($request->lastname){$user->lastname = $request->lastname;}
        if($request->email){$user->email = $request->email;}

        try{
            $user->update();
            return ["user"=>$user,"message" => "User updated successfully"];
        }catch(\Exception $e){
            return $e->getMessage();
        }

    }















}
