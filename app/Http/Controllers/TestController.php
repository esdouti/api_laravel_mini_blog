<?php

namespace App\Http\Controllers;
use App\Services\Livre;
use Illuminate\Http\Request;

class TestController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest');
    }

    public function index(Livre $livre)
    {
        $titre = $livre->getTitle();

        return view('test1', compact('titre'));
    }

}





