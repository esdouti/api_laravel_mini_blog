<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class AdminController extends Controller
{
    //
    function back_login(Request $request){
        $admin_data = array(
            'email'  => $request->get('email'),
            'password' => $request->get('password')
        );

        // dd($admin_data);
        $req = Auth::guard('admin')->attempt($admin_data);
        if ($req)
        {
            return redirect('/')->with('message', 'Bienvenue');
        }else{
            dd($req);
            return back()->with('error', 'Identifiants incorrects');
        }
    }
}
