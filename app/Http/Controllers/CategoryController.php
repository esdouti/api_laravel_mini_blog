<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;

class CategoryController extends Controller
{

    function categories() {
        $categories = Category::all();
        return view('categories',['categories'=>$categories]);
    }

    function new_cate(Request $request){
        $categorie = new Category;
        $categorie->name=$request->catename;
        $categorie->save();
        $categories = Category::all();
        return redirect('/categories');
    }

    function edit(Request $request){
        $id=$request->cat_id;
        $categorie=Category::find($id);
        $categorie->name=$request->fnom;
        $categorie->update();
        return redirect('/categories');
    }

    function delete(Request $request){
        $id=$request->cat_id;
        $categorie=Category::find($id);
        $categorie->delete();
        return redirect('/categories');
    }
}
