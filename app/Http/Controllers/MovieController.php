<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\Category;
use App\Models\User;
use DB;

class MovieController extends Controller
{


    function movies() {
        $categories = Category::all();
        $movies = DB::select('SELECT mo.id, mo.title, mo.description, ca.name FROM movies mo, categories ca WHERE mo.id_category=ca.id');
        return view('movies',['movies'=>$movies,'categories'=>$categories]);

    }

    function new_movie(Request $request){
        $movie = new Movie;
        $movie->title=$request->title;
        $movie->description=$request->description;
        $movie->id_category=$request->category;
        $movie->save();
        return redirect('/movies');
    }

    function delete(Request $request){
        // dd($request);
        $id=$request->cat_id;
        $movie=Movie::find($id);
        $movie->delete();
        return redirect('/movies');
    }

    function edit(Request $request){
        $id=$request->cat_id;
        $movie=Movie::find($id);
        $movie->title=$request->fnom;
        $movie->description=$request->desc;
        $movie->update();
        return redirect('/movies');
    }
}
