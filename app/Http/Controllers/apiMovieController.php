<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Movie;
use App\Models\Category;
use DB;

class apiMovieController extends Controller
{
    //
    // function getData(){
    //     $movies = DB::select('SELECT mo.id, mo.title, mo.description,mo.created_at, ca.name as cat_name,ca.id as cat_id FROM movies mo, categories ca WHERE mo.id_category=ca.id');
    //     return $movies;
    // }

    function getData2($id=null){
        if($id===null){
            $movies = DB::select('SELECT mo.id, mo.title, mo.description,mo.created_at, ca.name as cat_name,ca.id as cat_id FROM movies mo, categories ca WHERE mo.id_category=ca.id');
            return $movies;
        }else{
            $movies = DB::select("SELECT mo.id, mo.title, mo.description,mo.created_at, ca.name as cat_name,ca.id as cat_id FROM movies mo, categories ca WHERE mo.id=$id and mo.id_category=ca.id");
            return $movies;
        }
        // return $id?User::find($id):User::all();
    }

    function getData3($cat){
        return Movie::where('id_category',$cat)->get();
    }
}
