<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class RatingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $client3=new \App\Models\Rating();
           $client3->rate = 6;
           $client3->id_user1 = 2;
           $client3->id_movie = 4;
           $client3->save();

    }
}
