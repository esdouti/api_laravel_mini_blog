<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\Models\Admin;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $Admin1=new Admin();
        $Admin1->firstname = 'Espoir';
        $Admin1->lastname = 'DOUTI';
        $Admin1->email = 'admin@admin.com';
        $Admin1->password = Hash::make('123456admin');
        $Admin1->picture ='espoir.jpg';
        $Admin1->role ='superadmin';
        $comb = Hash::make("123");
        $shfl = str_shuffle($comb);
        $Admin1->ref = substr($shfl,0,25);
        $Admin1->save();

    }
}
