<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $client3=new \App\Models\User();
           $client3->firstname = 'Es';
           $client3->lastname = 'Jr';
           $client3->email = 'pm1@pm.com';
           $client3->password = Hash::make('123');
           $client3->picture = 'pdefault.png';
           $client3->save();

        $client4=new \App\Models\User();
           $client4->firstname = 'Es4';
           $client4->lastname = 'Jr4';
           $client4->email = 'pm2@pm.com';
           $client4->password = Hash::make('123');
           $client4->picture = 'pdefault.png';

           $client4->save();

        $client5=new \App\Models\User();
           $client5->firstname = 'Es5';
           $client5->lastname = 'Jr5';
           $client5->email = 'pm3@pm.com';
           $client5->password = Hash::make('123');
           $client5->picture = 'pdefault.png';
           $client5->save();

    }
}
