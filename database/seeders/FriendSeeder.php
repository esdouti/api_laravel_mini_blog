<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class FriendSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $client3=new \App\Models\Friend();
           $client3->id_user1 = 2;
           $client3->id_user2 = 4;
           $client3->save();

    }
}
